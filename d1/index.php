<?php 

	$tasks = ["Get it", "Bake HTML", "Eat CSS", "Learn PHP"];

	if(isset($_GET['index'])){
	// echo $_GET[''];
	// var_dump($_GET);

		$indexGet = $_GET['index'];
		echo "The retrieved task from get is $tasks[$indexGet]<br>";

	}
	if(isset($_POST['index'])){
	// echo $_POST[''];
	// var_dump($_POST);
		$indexPOST = $_POST['index'];
		echo "The retrieved task from POST is $tasks[$indexPOST]<br>";
	}

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>s05 client-server communication (GET and POST</title>
</head>
<body>
	<h1>Task index from GET</h1>
	<form method="GET">

		<select name="index" required>
			<option value="0">0</option>
			<option value="1">1</option>
			<option value="2">2</option>
			<option value="3">3</option>
		</select>

		<button type="submit">GET</button>
	</form>

	<h1>Task index from POST</h1>
	<form method="POST">
		<select name="index" required>
			<option value="0">0</option>
			<option value="1">1</option>
			<option value="2">2</option>
			<option value="3">3</option>
		</select>

		<button type="submit">POST</button>
	</form>
</body>
</html>